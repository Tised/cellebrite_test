package tiseddev.com.weatherclient.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by dmytro_vodnik on 5/14/16.
 * working on WeatherClient project
 */
public class DateConverter {

    public static String convert(long timestamp) {
        Calendar mydate = Calendar.getInstance();
        mydate.setTimeInMillis(timestamp * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
        return sdf.format(mydate.getTime());
    }
}
