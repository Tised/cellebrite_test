package tiseddev.com.weatherclient.utils;

/**
 * Created by dmytro_vodnik on 5/14/16.
 * working on WeatherClient project
 */
public class Const {

    public static final String LOADING = "loading";
    public static final String LOADED = "loaded";
    public static final String ERROR = "error";
    public static final String PROGRESS_INTENT = "progress";
    public static final String NEW_INFO_INTENT = "newInfo";
    static String city = "Odessa";
    static String country = "UA";

    public static String predefinedCity = city + "," + country;
}
