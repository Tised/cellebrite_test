package tiseddev.com.weatherclient.utils;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tiseddev.com.weatherclient.interfaces.WeatherApiInterface;
import tiseddev.com.weatherclient.models.WeatherAnswer;


/**
 * Created by dmytro_vodnik on 5/14/16.
 * working on WeatherApiClient project
 */
public class WeatherApiClient {

    private static final String TAG = "WEATHER API CLIENT";
    private static WeatherApiClient weatherApiClient;
    private WeatherApiInterface weatherApiInterface;

    public static WeatherApiClient init() {

        if (weatherApiClient == null)
            weatherApiClient = new WeatherApiClient();

        return weatherApiClient;
    }

    private WeatherApiClient() {

        Gson gson = new GsonBuilder()
                .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        weatherApiInterface = retrofit.create(WeatherApiInterface.class);
    }

    public Call<WeatherAnswer> getWeatherInfo(String city, String units, String apiKey) {

        Log.d(TAG, "get weather info started");

        return weatherApiInterface.getWeatherInfo(city, units, apiKey);
    }
}
