package tiseddev.com.weatherclient.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.sql.SQLException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tiseddev.com.weatherclient.R;
import tiseddev.com.weatherclient.models.WeatherAnswer;
import tiseddev.com.weatherclient.utils.Const;
import tiseddev.com.weatherclient.utils.DBUtils.HelperFactory;
import tiseddev.com.weatherclient.utils.WeatherApiClient;

public class DownloadService extends IntentService {

    private static final String TAG = "DOWNLOAD SERVICE";
    LocalBroadcastManager broadcastManager;

    public DownloadService() {
        super("MyTestService");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        broadcastManager = LocalBroadcastManager.getInstance(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // Do the task here
        Log.i("MyTestService", "Service running");

        WeatherApiClient weatherApiClient = WeatherApiClient.init();

        Call<WeatherAnswer> call = weatherApiClient.getWeatherInfo(Const.predefinedCity, "metric",
                getString(R.string.weather_app_key));

        showProgress(Const.LOADING);
        call.enqueue(new Callback<WeatherAnswer>() {
            @Override
            public void onResponse(Call<WeatherAnswer> call, Response<WeatherAnswer> response) {

                WeatherAnswer weatherAnswer = response.body();

                Log.d(TAG, "weather answer === " + weatherAnswer);

                try {

                    weatherAnswer.setLoadDate(System.currentTimeMillis());

                    Log.d(TAG, "adding new weather answer to DB = " + weatherAnswer);

                    HelperFactory.getHelper().getWeatherAnswerDAO().create(weatherAnswer);

                    sendToUI(weatherAnswer);
                    showProgress(Const.LOADED);
                } catch (SQLException e) {
                    Log.e(TAG, "error while add new weather to DB " + Log.getStackTraceString(e));
                }
            }

            @Override
            public void onFailure(Call<WeatherAnswer> call, Throwable t) {

                Log.e(TAG, "weather error = " + Log.getStackTraceString(t));
                showProgress(Const.ERROR);
            }
        });
    }

    private void showProgress(String type) {

        Intent intent = new Intent(Const.PROGRESS_INTENT);

        intent.putExtra("progressType", type);

        broadcastManager.sendBroadcast(intent);
    }

    private void sendToUI(WeatherAnswer weatherAnswer) {

        Intent intent = new Intent(Const.NEW_INFO_INTENT);

        if (weatherAnswer!=null) {

            intent.putExtra("weatherInfo", weatherAnswer);
        }

        broadcastManager.sendBroadcast(intent);
    }
}