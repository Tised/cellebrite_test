package tiseddev.com.weatherclient.models.weather_models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dmytro_vodnik on 5/14/16.
 * working on WeatherClient project
 */
@DatabaseTable
public class Sys {

    public double getMessage() {
        return message;
    }

    public void setMessage(double message) {
        this.message = message;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public long getSunrise() {
        return sunrise;
    }

    public void setSunrise(long sunrise) {
        this.sunrise = sunrise;
    }

    public long getSunset() {
        return sunset;
    }

    public void setSunset(long sunset) {
        this.sunset = sunset;
    }

    @DatabaseField(generatedId=true)
    public int id;

    @SerializedName("message")
    @DatabaseField
    double message;

    @SerializedName("country")
    @DatabaseField
    String country;

    @SerializedName("sunrise")
    @DatabaseField
    long sunrise;

    @SerializedName("sunset")
    @DatabaseField
    long sunset;

    @Override
    public String toString() {
        return "Sys{" +
                "message=" + message +
                ", country='" + country + '\'' +
                ", sunrise=" + sunrise +
                ", sunset=" + sunset +
                '}';
    }
}
