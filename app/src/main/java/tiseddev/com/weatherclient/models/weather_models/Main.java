package tiseddev.com.weatherclient.models.weather_models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dmytro_vodnik on 5/14/16.
 * working on WeatherClient project
 */
@DatabaseTable
public class Main {

    @DatabaseField(generatedId=true)
    public int id;

    @SerializedName("temp")
    @DatabaseField
    double temp;

    @SerializedName("pressure")
    @DatabaseField
    double pressure;

    @SerializedName("humidity")
    @DatabaseField
    double humidity;

    @SerializedName("temp_min")
    @DatabaseField
    double tempMin;

    @SerializedName("temp_max")
    @DatabaseField
    double tempMax;

    @SerializedName("sea_level")
    @DatabaseField
    double seaLevel;

    public double getGroundLevel() {
        return groundLevel;
    }

    public void setGroundLevel(double groundLevel) {
        this.groundLevel = groundLevel;
    }

    public double getSeaLevel() {
        return seaLevel;
    }

    public void setSeaLevel(double seaLevel) {
        this.seaLevel = seaLevel;
    }

    public double getTempMax() {
        return tempMax;
    }

    public void setTempMax(double tempMax) {
        this.tempMax = tempMax;
    }

    public double getTempMin() {
        return tempMin;
    }

    public void setTempMin(double tempMin) {
        this.tempMin = tempMin;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    @SerializedName("grnd_level")
    double groundLevel;

    @Override
    public String toString() {
        return "Main{" +
                "temp=" + temp +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                ", tempMin=" + tempMin +
                ", tempMax=" + tempMax +
                ", seaLevel=" + seaLevel +
                ", groundLevel=" + groundLevel +
                '}';
    }
}
