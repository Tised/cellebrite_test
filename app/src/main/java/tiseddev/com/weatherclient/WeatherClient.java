package tiseddev.com.weatherclient;

import android.app.Application;
import android.util.Log;

import tiseddev.com.weatherclient.utils.DBUtils.HelperFactory;

/**
 * Created by dmytro_vodnik on 5/13/16.
 * working on WeatherApiClient project
 */
public class WeatherClient extends Application {

    private static final String APP_TAG = "WEATHER CLIENT";

    @Override
    public void onCreate(){
        super.onCreate();

        Log.d(APP_TAG, "app started");
        HelperFactory.setHelper(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        super.onTerminate();
    }
}
