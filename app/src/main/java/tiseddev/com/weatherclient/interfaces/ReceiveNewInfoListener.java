package tiseddev.com.weatherclient.interfaces;

import tiseddev.com.weatherclient.models.WeatherAnswer;

/**
 * Created by dmytro_vodnik on 5/14/16.
 * working on WeatherClient project
 */
public interface ReceiveNewInfoListener {

    void newInfoReceived(WeatherAnswer weatherAnswer);
    void loadingStarted();
    void loadingFinished();
    void loadingError();
}