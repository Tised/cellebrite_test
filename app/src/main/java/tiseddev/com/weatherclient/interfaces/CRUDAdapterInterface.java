package tiseddev.com.weatherclient.interfaces;

import java.util.List;

/**
 * Created by dmytro_vodnik on 4/3/16.
 * working on WaiterApp project
 */
public interface CRUDAdapterInterface<T> {

    T getItem(int position);
    void addItem(T item);
    void removeItem(T item);
    void addItems(List<T> items);
    void clearList();
    void updateItem(T oldItem, T item);
    void showItem(T item);
    List<T> getItems();
}
