Weather client 

This app lets you to receive actual information for Odessa (Ukraine) city from openweathermap.org/current API

For requests to API was used Retrofit v2
For Database was used ORMLite

Also for data binding between view and XML was used Android data binding library.

At the start of app is retrieving the data at once (for actual information to current time) and then every 10 min.

App restart service on phone reboot